
# Precision Thermometer & Hygrometer

![Showcase image](showcase.jpg)

If you're interested in some thoughts and images of the project, have a look at the [corresponding page on my website](https://maxkl.de/projects/electronics/thermometer/).

