
#set terminal dumb

#set title "EnergyTrace" font ",20"
set xlabel "Seconds"
set ylabel "Current"
#set yrange [0:10e-06]
set xrange [0:10]

plot "energytrace.txt" with lines;

pause mouse close
