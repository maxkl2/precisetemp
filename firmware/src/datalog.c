
#include "datalog.h"

#include <msp430.h>

#include <stdlib.h>

#include "tmp117.h"
#include "htu31d.h"
#include "uart.h"

#define SHIFT_TEMP TMP117_TEMP_SHIFT
#define SHIFT_TEMP_LOG 6

#define SHIFT_HUM HTU31D_RH_SHIFT
#define SHIFT_HUM_LOG 4

// Upper FRAM region is exactly 32 kiB (32767 B) bytes but .upper.data always uses 2 B, therefore 32765 B remain
#define LOG_MEMORY_LEN 32765

#define HEADER_MAGIC 0xda
#define MARKER_ABS 0x80

struct log_header {
    uint8_t magic;
    uint8_t options;
    uint16_t len;
} __attribute__((packed));
typedef struct log_header log_header_t;
#define HEADER_LEN sizeof(struct log_header)

// Place in non-volatile FRAM
static uint8_t log_memory[LOG_MEMORY_LEN] __attribute__((section(".upper.text")));

static uint16_t next_index = 0;
static log_header_t *current_header = NULL;
static enum datalog_interval current_interval;
static uint8_t current_interval_seconds;
static enum datalog_type current_type;
static bool logging = false;

static uint8_t counter;

static bool first_temp;
static bool first_hum;
static int16_t prev_temp;
static int16_t prev_hum;
static int16_t error_acc_temp;
static int16_t error_acc_hum;

static int32_t cic_sum_temp;
static int32_t cic_sum_hum;

void datalog_init() {
    uint16_t index = 0;
    log_header_t *header = (log_header_t *) &log_memory[index];
    while (header->magic == HEADER_MAGIC) {
        index += header->len;
        header = (log_header_t *) &log_memory[index];
    }
    next_index = index;
}

static uint8_t interval_to_seconds(enum datalog_interval interval) {
    switch (interval) {
        case DL_INTERVAL_1S: return 1;
        case DL_INTERVAL_2S: return 2;
        case DL_INTERVAL_5S: return 5;
        case DL_INTERVAL_10S: return 10;
        case DL_INTERVAL_30S: return 30;
        case DL_INTERVAL_60S: return 60;
    }
    return 1;
}

static void memory_write_enable() {
    // Disable Program/Main FRAM write protection
    SYSCFG0 = FRWPPW | DFWP;
}

static void memory_write_disable() {
    // Reenable FRAM write protection
    SYSCFG0 = FRWPPW | DFWP | PFWP;
}

void datalog_clear() {
    if (logging) {
        return;
    }

    memory_write_enable();

    uint16_t index = 0;
    log_header_t *header = (log_header_t *) &log_memory[index];
    while (header->magic == HEADER_MAGIC) {
        index += header->len;

        // Invalidate header
        header->magic = 0;

        if (index >= LOG_MEMORY_LEN) {
            // Bad length, would exceed memory size
            break;
        }

        header = (log_header_t *) &log_memory[index];
    }

    next_index = 0;
    current_header = NULL;

    memory_write_disable();

    uart_puts("All logs cleared\r\n");
}

void datalog_start(enum datalog_interval interval, enum datalog_type type) {
    if (current_header != NULL) {
        // Already logging

        // TODO: do sth else?
        return;
    }

    if (next_index + HEADER_LEN > LOG_MEMORY_LEN) {
        // Memory full

        // TODO: report
        return;
    }

    memory_write_enable();

    log_header_t *header = (log_header_t *) &log_memory[next_index];
    header->magic = HEADER_MAGIC;
    header->options = (type & 0x3) | (interval & 0x7) << 2;
    header->len = HEADER_LEN;

    memory_write_disable();

    next_index += HEADER_LEN;

    current_header = header;
    current_interval = interval;
    current_interval_seconds = interval_to_seconds(interval);
    current_type = type;

    counter = 0;

    cic_sum_temp = 0;
    cic_sum_hum = 0;

    first_temp = true;
    first_hum = true;

    uart_puts("Logging started\r\n");

    logging = true;
}

bool datalog_logging() {
    return logging;
}

static int16_t fpround(int16_t v, size_t shift) {
    int16_t val_05 = 1 << (shift - 1);
    return (v + val_05) >> shift;
}

static void store_u8(uint8_t value) {
    log_memory[next_index] = value;
    next_index++;
}

static void store_u16(uint16_t value) {
    // *((uint16_t *) &log_memory[next_index]) = value;
    log_memory[next_index] = value & 0xff;
    log_memory[next_index + 1] = value >> 8;
    next_index += 2;
}

static bool check_memory(uint16_t needed) {
    if (next_index + needed > LOG_MEMORY_LEN) {
        logging = false;
        return true;
    }

    return false;
}

static void store_abs_no_marker(uint16_t value) {
    if (check_memory(2)) {
        return;
    }

    memory_write_enable();

    store_u16(value);
    current_header->len += 2;

    memory_write_disable();
}

static void store_abs(uint16_t value) {
    if (check_memory(3)) {
        return;
    }

    memory_write_enable();

    store_u8(MARKER_ABS);
    store_u16(value);
    current_header->len += 3;

    memory_write_disable();
}

static void store_rel(uint8_t value) {
    if (check_memory(1)) {
        return;
    }

    memory_write_enable();

    store_u8(value);
    current_header->len += 1;

    memory_write_disable();
}

static void store_temp(int16_t temp) {
    // temp: Q8.7
    // prev_temp: Q8.7
    // error_acc_temp: Q8.7

    if (first_temp) {
        first_temp = false;

        store_abs_no_marker(temp);

        error_acc_temp = 0;
    } else {
        // Q8.7
        int16_t diff = temp - prev_temp;

        // Q9.6
        int16_t diff_shifted = fpround(diff, SHIFT_TEMP - SHIFT_TEMP_LOG);

        // Q9.6
        int16_t diff_unshifted = diff_shifted << (SHIFT_TEMP - SHIFT_TEMP_LOG);

        // Q8.7
        int16_t error = diff - diff_unshifted;

        error_acc_temp += error;
        // Q9.6
        int16_t error_acc_shifted = error_acc_temp >> (SHIFT_TEMP - SHIFT_TEMP_LOG);
        if (error_acc_shifted != 0) {
            diff_shifted += error_acc_shifted;
            // Q8.7
            int16_t error_acc_unshifted = error_acc_shifted << (SHIFT_TEMP - SHIFT_TEMP_LOG);
            error_acc_temp -= error_acc_unshifted;
        }

        // Q1.6
        int8_t diff_8bit = diff_shifted;
        if (diff_shifted != diff_8bit || ((uint8_t) diff_8bit) == MARKER_ABS) {
            // Diff doesn't fit into 8 bit

            store_abs(temp);

            error_acc_temp = 0;
        } else {
            store_rel(diff_8bit);
        }
    }

    prev_temp = temp;
}

static void store_hum(int16_t hum) {
    // hum: Q8.7
    // prev_hum: Q8.7
    // error_acc_hum: Q8.7

    if (first_hum) {
        first_hum = false;

        store_abs_no_marker(hum);

        error_acc_hum = 0;
    } else {
        // Q8.7
        int16_t diff = hum - prev_hum;

        // Q9.6
        int16_t diff_shifted = fpround(diff, SHIFT_HUM - SHIFT_HUM_LOG);

        // Q9.6
        int16_t diff_unshifted = diff_shifted << (SHIFT_HUM - SHIFT_HUM_LOG);

        // Q8.7
        int16_t error = diff - diff_unshifted;

        error_acc_hum += error;
        // Q9.6
        int16_t error_acc_shifted = error_acc_hum >> (SHIFT_HUM - SHIFT_HUM_LOG);
        if (error_acc_shifted != 0) {
            diff_shifted += error_acc_shifted;
            // Q8.7
            int16_t error_acc_unshifted = error_acc_shifted << (SHIFT_HUM - SHIFT_HUM_LOG);
            error_acc_hum -= error_acc_unshifted;
        }

        // Q1.6
        int8_t diff_8bit = diff_shifted;
        if (diff_shifted != diff_8bit || ((uint8_t) diff_8bit) == MARKER_ABS) {
            // Diff doesn't fit into 8 bit

            store_abs(hum);

            error_acc_hum = 0;
        } else {
            store_rel(diff_8bit);
        }
    }

    prev_hum = hum;
}

static void log_temp(int16_t temp, bool store, uint8_t interval_seconds) {
    cic_sum_temp += temp;

    if (store) {
        int32_t avg = cic_sum_temp / interval_seconds;
        cic_sum_temp = 0;

        store_temp(avg);
    }
}

static void log_hum(int16_t hum, bool store, uint8_t interval_seconds) {
    cic_sum_hum += hum;

    if (store) {
        int32_t avg = cic_sum_hum / interval_seconds;
        cic_sum_hum = 0;

        store_hum(avg);
    }
}

void datalog_log(int16_t temp, int16_t hum) {
    if (!logging) {
        return;
    }

    bool store = false;

    counter++;
    if (counter == current_interval_seconds) {
        counter = 0;

        store = true;
    }

    if (current_type == DL_TYPE_TEMP || current_type == DL_TYPE_BOTH) {
        log_temp(temp, store, current_interval_seconds);
    }

    if (current_type == DL_TYPE_HUM || current_type == DL_TYPE_BOTH) {
        log_hum(hum, store, current_interval_seconds);
    }
}

uint16_t datalog_estimate_duration(bool append, enum datalog_type type, enum datalog_interval interval) {
    uint16_t bytes_available;
    if (append) {
        bytes_available = LOG_MEMORY_LEN - next_index;
    } else {
        bytes_available = LOG_MEMORY_LEN;
    }

    uint16_t n_samples;
    if (type == DL_TYPE_BOTH) {
        n_samples = bytes_available / 2;
    } else {
        n_samples = bytes_available;
    }

    uint16_t samples_per_hour;
    switch (interval) {
        case DL_INTERVAL_1S: samples_per_hour = 3600; break;
        case DL_INTERVAL_2S: samples_per_hour = 1800; break;
        case DL_INTERVAL_5S: samples_per_hour = 720; break;
        case DL_INTERVAL_10S: samples_per_hour = 360; break;
        case DL_INTERVAL_30S: samples_per_hour = 120; break;
        case DL_INTERVAL_60S: samples_per_hour = 60; break;
    }

    uint16_t hours = n_samples / samples_per_hour;

    return hours;
}

static uint16_t transmit_index;
static uint8_t nibble;
static bool transmitting;

void datalog_initiate_transmit() {
    transmit_index = 0;
    nibble = 0;

    transmitting = true;
}

void datalog_fill_dummy_data() {
    memory_write_enable();

    for (uint16_t i = 0; i < LOG_MEMORY_LEN; i++) {
        log_memory[i] = i & 0xff;
    }

    memory_write_disable();
}

bool datalog_transmit() {
    if (!transmitting) {
        return true;
    }

    while (transmit_index < LOG_MEMORY_LEN && transmit_index < next_index) {
        uint8_t value = log_memory[transmit_index];
        if (nibble == 0) {
            size_t written = uart_print_hex((value >> 4) & 0xf);
            if (written == 0) {
                return false;
            }

            nibble = 1;
        } else {
            size_t written = uart_print_hex(value & 0xf);
            if (written == 0) {
                return false;
            }

            nibble = 0;

            transmit_index++;
        }
    }

    transmitting = false;

    return true;
}
