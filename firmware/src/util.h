
#pragma once

#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>

typedef _ssize_t ssize_t;

void decimal_to_digits(int16_t value, size_t shift, size_t digit_count, ssize_t ones_index, uint8_t *out);
