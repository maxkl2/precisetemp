
#include "uart.h"

#include <msp430.h>
#include <stdbool.h>

#define UART_BAUD_RATE 115200
#define UART_TX_BUFFER_SIZE 128
#define UART_TX_BUFFER_INDEX_MASK (UART_TX_BUFFER_SIZE - 1)
#define UART_RX_BUFFER_SIZE 128
#define UART_RX_BUFFER_INDEX_MASK (UART_TX_BUFFER_SIZE - 1)

static char uart_tx_buffer[UART_TX_BUFFER_SIZE];
static size_t uart_tx_buffer_write_index = 0;
static volatile size_t uart_tx_buffer_read_index = 0;
static char uart_rx_buffer[UART_RX_BUFFER_SIZE];
static volatile size_t uart_rx_buffer_write_index = 0;
static size_t uart_rx_buffer_read_index = 0;
static volatile bool uart_tx_active = false;

/**
 * P1.4: UCA0TXD (UART to USB)
 * P1.5: UCA0RXD (UART to USB)
 */
void uart_init_io() {
    P1SEL0 |= BIT4 | BIT5;
}

void uart_enable() {
    UCA0CTLW0 = UCSWRST;
    // Character length: 8 bit, Stop bits: 1, LSB first, Parity: odd
    UCA0CTLW0 |= UCSSEL__SMCLK | UCPEN;
    UCA0CTLW1 = UCGLIT_3;
    // Configuration for 115200 bd with BRCLK = 8 MHz
    // UCOS16 = 1, UCBRx = 4, UCBRFx = 5, UCBRSx = 0x55
    UCA0BRW = 4;
    UCA0MCTLW = UCOS16 | (5 << 4) | (0x55 << 8);
    UCA0CTLW0 &= ~UCSWRST;

    uart_tx_active = false;

    UCA0IFG = 0;
    UCA0IE = UCRXIE | UCTXIE;
}

void uart_disable() {
    UCA0CTLW0 |= UCSWRST;
    uart_tx_active = false;
}

size_t uart_putc(char c) {
    size_t ret = 0;
    __dint();
    if (uart_tx_active) {
        // Transmission is in progress, character will be taken out of the buffer when the current one has been sent
        size_t read_index = uart_tx_buffer_read_index;
        size_t write_index = uart_tx_buffer_write_index;
        if (((write_index + 1) & UART_TX_BUFFER_INDEX_MASK) == read_index) {
            // Buffer full
            ret = 0;
            goto reenable_interrupts;
        }
        uart_tx_buffer[write_index] = c;
        uart_tx_buffer_write_index = (write_index + 1) & UART_TX_BUFFER_INDEX_MASK;
        ret = 1;
        goto reenable_interrupts;
    } else {
        // Transmission not in progress
        uart_tx_active = true;
        UCA0TXBUF = c;
        ret = 1;
        goto reenable_interrupts;
    }
reenable_interrupts:
    __eint();
    return ret;
}

size_t uart_puts(char *str) {
    size_t len = 0;
    while (*str != '\0') {
        size_t transmitted = uart_putc(*str);
        if (transmitted == 0) {
            break;
        }
        str++;
        len++;
    }
    return len;
}

static size_t uart_tx_buffer_getc(char *c_out) {
    size_t read_index = uart_tx_buffer_read_index;
    if (read_index == uart_tx_buffer_write_index) {
        return 0;
    }
    *c_out = uart_tx_buffer[read_index];
    uart_tx_buffer_read_index = (read_index + 1) & UART_TX_BUFFER_INDEX_MASK;
    return 1;
}

static size_t uart_rx_buffer_putc(char c) {
    size_t write_index = uart_rx_buffer_write_index;
    if (((write_index + 1) & UART_RX_BUFFER_INDEX_MASK) == uart_rx_buffer_read_index) {
        // Buffer full
        return 0;
    }
    uart_rx_buffer[write_index] = c;
    uart_rx_buffer_write_index = (write_index + 1) & UART_RX_BUFFER_INDEX_MASK;
    return 1;
}

size_t uart_getc(char *c_out) {
    // No need to __dint() since uart_rx_buffer_write_index is read in a single cycle
    size_t write_index = uart_rx_buffer_write_index;
    size_t read_index = uart_rx_buffer_read_index;
    if (read_index == write_index) {
        return 0;
    }
    *c_out = uart_rx_buffer[read_index];
    uart_rx_buffer_read_index = (read_index + 1) & UART_RX_BUFFER_INDEX_MASK;
    return 1;
}

size_t uart_print_hex(uint8_t value) {
    if (value < 10) {
        return uart_putc('0' + value);
    } else if (value < 16) {
        return uart_putc('A' + (value - 10));
    }
    return 0;
}

size_t uart_print_u8_hex(uint8_t value) {
    size_t chars_written = uart_print_hex((value >> 4) & 0xf);
    if (chars_written > 0) {
        // Only print second char if first went through
        return chars_written + uart_print_hex(value & 0xf);
    } else {
        return 0;
    }
}

void uart_print_u16_hex(uint16_t value) {
    uart_print_hex(value >> 12);
    uart_print_hex((value >> 8) & 0xf);
    uart_print_hex((value >> 4) & 0xf);
    uart_print_hex(value & 0xf);
}

__attribute__((interrupt(EUSCI_A0_VECTOR)))
void isr_USCI_A0(void) {
    uint16_t source = UCA0IV;
    switch (source) {
        case UCIV__UCRXIFG: {
            uint16_t stat = UCA0STATW;
            // Reading from UCAxRXBUF automatically clears any receive error flags
            uint8_t data = UCA0RXBUF;
            // Discard data if any error occurred
            if (!(stat & UCRXERR)) {
                uart_rx_buffer_putc(data);
            }

            __low_power_mode_off_on_exit();
            break;
        }
        case UCIV__UCTXIFG: {
            char next_c;
            size_t l = uart_tx_buffer_getc(&next_c);
            if (l > 0) {
                UCA0TXBUF = next_c;
            } else {
                uart_tx_active = false;
            }
            break;
        }
    }
}
