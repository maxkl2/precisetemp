
#pragma once

#include <stddef.h>
#include <stdint.h>

void uart_init_io();
void uart_enable();
void uart_disable();
size_t uart_putc(char c);
size_t uart_puts(char *str);
size_t uart_getc(char *c_out);
size_t uart_print_hex(uint8_t value);
size_t uart_print_u8_hex(uint8_t value);
void uart_print_u16_hex(uint16_t value);
