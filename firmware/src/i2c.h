
#pragma once

#include <stddef.h>
#include <stdint.h>

enum i2c_result {
    I2C_OK = 0,
    I2C_LEN_INVALID,
    I2C_ADDR_NACK
};
typedef enum i2c_result i2c_result_t;

void i2c_init_io();
void i2c_init();
i2c_result_t i2c_write(uint8_t addr, size_t len, uint8_t *data);
i2c_result_t i2c_write_u8(uint8_t addr, uint8_t data);
i2c_result_t i2c_write_u8_u16_be(uint8_t addr, uint8_t data0, uint16_t data1);
i2c_result_t i2c_read(uint8_t addr, size_t len, uint8_t *data_out);
i2c_result_t i2c_read_u8(uint8_t addr, uint8_t *data_out);
i2c_result_t i2c_read_u16_be(uint8_t addr, uint16_t *data_out);
