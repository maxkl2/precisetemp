
#include "htu31d.h"

#include <msp430.h>

#include "clock_system.h"
#include "i2c.h"
#include "uart.h"

#define I2C_ADDR_HTU31 0x40

#define HTU31D_CMD_CONV(osrt, osrrh) (0x40 | (osrrh) << 3 | (osrt) << 1)
#define HTU31D_CMD_READ_T_RH 0x00
#define HTU31D_CMD_READ_RH 0x10
#define HTU31D_CMD_RESET 0x1E
#define HTU31D_CMD_HEATER_ON 0x04
#define HTU31D_CMD_HEATER_OFF 0x02
#define HTU31D_CMD_READ_SN 0x0A
#define HTU31D_CMD_READ_DIAG 0x08

/**
 * P1.1: Output (HTU310 RST)
 */
void htu31d_init_io() {
    // Output and driven low by default
}

int htu31d_init() {
    i2c_result_t i2c_res;

    // Exit reset
    P1OUT |= BIT1;

    // Wait until out of reset
    __delay_cycles(T_MS * 15);

    i2c_res = i2c_write_u8(I2C_ADDR_HTU31, HTU31D_CMD_READ_SN);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    uint8_t sn_buf[3];
    i2c_res = i2c_read(I2C_ADDR_HTU31, 3, sn_buf);
    if (i2c_res != I2C_OK) {
        return 1;
    }
    uart_puts("HTU31D SN: ");
    uart_print_u8_hex(sn_buf[0]);
    uart_print_u16_hex((uint16_t) sn_buf[1] << 8 | sn_buf[2]);
    uart_puts("\r\n");

    return 0;
}

static int16_t htu31d_convert_humidity(uint16_t raw) {
    // raw: 0 to 65535
    // * 51200 / 65535
    // * 10240 / 13107
    // out: 0 to 127.xxx (8.8 fixed point)
    return (uint32_t) raw * 5120l / 13107l;
}

int16_t htu31d_int2rh(int16_t v) {
    return v << HTU31D_RH_SHIFT;
}

int htu31d_trigger_conversion() {
    i2c_result_t i2c_res;

    i2c_res = i2c_write_u8(I2C_ADDR_HTU31, HTU31D_CMD_CONV(0, 3));
    if (i2c_res != I2C_OK) {
        return 1;
    }

    return 0;
}

int htu31d_read_humidity(int16_t *humidity_out) {
    i2c_result_t i2c_res;

    i2c_res = i2c_write_u8(I2C_ADDR_HTU31, HTU31D_CMD_READ_RH);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    uint16_t humidity_raw;
    i2c_res = i2c_read_u16_be(I2C_ADDR_HTU31, &humidity_raw);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    *humidity_out = htu31d_convert_humidity(humidity_raw);

    return 0;
}
