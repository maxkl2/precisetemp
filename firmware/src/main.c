
#include <msp430.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "uart.h"
#include "lcd.h"
#include "clock_system.h"
#include "i2c.h"
#include "tmp117.h"
#include "htu31d.h"
#include "heatindex.h"
#include "datalog.h"

// Long press if held longer than (13 / 512Hz) * 20 = 508ms (13 is TA1CCR0 + 1)
#define BTN_PRESS_COUNT_LONG 20

enum btn_press {
    BTN_PRESS_NONE = 0,
    BTN_PRESS_SHORT,
    BTN_PRESS_LONG
};

enum disp_state {
    DISP_STATE_TEMP = 0,
    DISP_STATE_RH,
    DISP_STATE_HEATINDEX,

    DISP_STATE_HOLD_TEMP,
    DISP_STATE_HOLD_RH,
    DISP_STATE_HOLD_HEATINDEX,

    DISP_STATE_MENU_DL_APPEND,
    DISP_STATE_MENU_DL_NEW,
    DISP_STATE_MENU_DL_TRANSMIT,
    DISP_STATE_MENU_BACK,

    DISP_STATE_MENU_DL_OPTIONS_TYPE_TEMP,
    DISP_STATE_MENU_DL_OPTIONS_TYPE_RH,
    DISP_STATE_MENU_DL_OPTIONS_TYPE_BOTH,
    DISP_STATE_MENU_DL_OPTIONS_TYPE_BACK,

    DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S, // 14
    DISP_STATE_MENU_DL_OPTIONS_INTERVAL_2S,
    DISP_STATE_MENU_DL_OPTIONS_INTERVAL_5S, // 16
    DISP_STATE_MENU_DL_OPTIONS_INTERVAL_10S,
    DISP_STATE_MENU_DL_OPTIONS_INTERVAL_30S,
    DISP_STATE_MENU_DL_OPTIONS_INTERVAL_60S,
    DISP_STATE_MENU_DL_OPTIONS_INTERVAL_BACK,

    DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO,
    DISP_STATE_MENU_DL_OPTIONS_CONFIRM_BACK,
};

static volatile bool usb_connected = false;
static volatile bool tmp117_alert = false;
static volatile enum btn_press button_pressed = BTN_PRESS_NONE;

static volatile bool trigger_conversion = false;
static volatile bool read_humidity = false;

static int16_t last_temp = 0;
static int16_t last_rh = 0;

static enum disp_state disp_state = DISP_STATE_TEMP;

static void init_watchdog() {
    // TODO: trigger after just over 10ms (in case of 100Hz) to prevent LCD damage
}

static void set_up_io_defaults() {
    // The MSP430FR2xx family user's guide specifies:
    //   - Unused IOs should be configured as outputs and driven low to reduce power consumption
    //   - PxDIR, PxREN, PxOUT and PxIES have undefined values after power up

    P1DIR = 0xff;
    P1REN = 0x00;
    P1OUT = 0x00;
    P1IES = 0x00;

    P2DIR = 0xff;
    P2REN = 0x00;
    P2OUT = 0x00;
    P2IES = 0x00;

    P3DIR = 0xff;
    P3REN = 0x00;
    P3OUT = 0x00;
    P3IES = 0x00;

    P4DIR = 0xff;
    P4REN = 0x00;
    P4OUT = 0x00;
    P4IES = 0x00;

    P5DIR = 0xff;
    P5REN = 0x00;
    P5OUT = 0x00;
    P5IES = 0x00;
}

/**
 * P1.0: A0 (Battery voltage)
 * P1.6: Input, hi-Z (VBUS detect)
 * P2.2: Input, pull-up (TMP117 ALERT)
 * P3.5: Input, pull-up (Button)
 */
static void init_misc_io() {
    P1SEL0 |= BIT0;
    P1SEL1 |= BIT0;
    P1DIR &= ~BIT6;

    P2DIR &= ~BIT2;
    P2REN |= BIT2;
    P2OUT |= BIT2;
    P2IES |= BIT2;

    P3DIR &= ~BIT5;
    P3REN |= BIT5;
    P3OUT |= BIT5;
    P3IES |= BIT5;
}

static void enable_port_interrupts() {
    // Clear all PxIFGs to avoid erroneous port interrupts
    P1IFG = 0x00;
    P2IFG = 0x00;
    P3IFG = 0x00;
    P4IFG = 0x00;
    P5IFG = 0x00;

    P1IE = BIT6;
    P2IE = BIT2;
    P3IE = BIT5;
    P4IE = 0x00;
    P5IE = 0x00;
}

static void init_timers() {
    // Timer_A 1 is used to debounce the button
    TA1CTL = TACLR;
    // Timer clock is ACLK/64 = 512Hz
    TA1CTL |= ID__8 | TASSEL__ACLK;
    TA1EX0 = TAIDEX__8;
    // Set up for delay of ~25ms => 25ms * 512Hz = 12.8
    TA1CCR0 = (13 - 1);
    TA1CCTL0 = CCIE;

    // Timer_A 2 is used to trigger measurements in regular intervals
    TA2CTL = TACLR;
    // Timer clock is ACLK/64 = 512Hz
    TA2CTL |= ID__8 | TASSEL__ACLK;
    TA2EX0 = TAIDEX__8;
    // Trigger once per second: 512Hz / 1Hz = 512
    TA2CCR0 = (512 - 1);
    TA2CCTL0 = CCIE;
    // Separate interrupt for reading the HTU31D sensor after the conversion time (max. ~12ms): 15ms * 512Hz = 7.68
    TA2CCR1 = 8;
    TA2CCTL1 = CCIE;
}

static int init_sensors() {
    int res = 0;

    res = tmp117_init();
    if (res != 0) {
        return res;
    }

    res = htu31d_init();

    // Start regular sensor conversions
    TA2CTL |= MC__UP;

    return res;
}

static void disp_temp(int16_t temp) {
    if (temp < tmp117_int2temp(100)) {
        lcd_display_frac(temp, TMP117_TEMP_SHIFT, 2);
    } else {
        lcd_display_frac(temp, TMP117_TEMP_SHIFT, 1);
    }
}

static void disp_rh(int16_t rh) {
    if (rh < htu31d_int2rh(100)) {
        lcd_display_frac(rh, HTU31D_RH_SHIFT, 2);
    } else {
        lcd_display_frac(rh, HTU31D_RH_SHIFT, 1);
    }
}

static void disp_heatindex(int16_t temp, int16_t rh) {
    int8_t heatindex = 0;
    if (temp > 0) {
        heatindex = heatindex_get(temp >> TMP117_TEMP_SHIFT, rh >> HTU31D_RH_SHIFT);
    }
    lcd_display_number(heatindex, 0, 0);
}

int main(void) {
    int res;

    // Disable the watchdog timer
    WDTCTL = WDTPW | WDTHOLD;

    init_clocks();

    //init_watchdog();

    datalog_init();

    set_up_io_defaults();

    i2c_init_io();
    uart_init_io();
    htu31d_init_io();
    lcd_init_io();
    init_misc_io();

    // Disable the GPIO power-on default high-impedance mode to activate previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

    lcd_init();

    init_timers();

    // TODO: don't trigger sensor that's not in use
    // TODO: next up: battery voltage, watchdog, continuous data to pc, data logging to fram

    usb_connected = P1IN & BIT6;
    if (usb_connected) {
        P1IES |= BIT6;
    }

    // TODO: only enable UART when USB connected
    uart_enable();

    datalog_init();

    enable_port_interrupts();

    __eint();

    i2c_init();
    res = init_sensors();
    if (res != 0) {
        uart_puts("Sensor init failed\r\n");
        return 1;
    }

    // FIXME: DL_TYPE_HUM and DL_INTERVAL_2S wrong if following is commented:
    // uart_puts("DL_TYPE_TEMP=");
    // uart_print_u8_hex(DL_TYPE_TEMP);
    // uart_puts(", DL_TYPE_HUM=");
    // uart_print_u8_hex(DL_TYPE_HUM);
    // uart_puts(", DL_TYPE_BOTH=");
    // uart_print_u8_hex(DL_TYPE_BOTH);
    // uart_puts("\r\n");

    uart_puts("Initialization done\r\n");

    trigger_conversion = true;

    // TODO: start watchdog when properly running

    lcd_display_clear();
    lcd_load_data(false);
    lcd_enable();

    bool have_temp_for_log = false;
    bool have_hum_for_log = false;
    bool transmitting_log = false;

    bool datalog_append = false;
    enum datalog_type datalog_type = DL_TYPE_TEMP;
    enum datalog_interval datalog_interval = DL_INTERVAL_1S;

    bool dl_confirm_show_go = false;

    while (1) {
        lcd_update();

        // Read & write all volatile variables while interrupts are disabled
        __dint();

        bool local_trigger_conversion = trigger_conversion;
        trigger_conversion = false;

        bool local_tmp117_alert = tmp117_alert;
        tmp117_alert = false;

        bool local_read_humidity = read_humidity;
        read_humidity = false;

        enum btn_press local_button_pressed = button_pressed;
        button_pressed = BTN_PRESS_NONE;

        bool local_usb_connected = usb_connected;

        __eint();

        if (local_trigger_conversion) {
            tmp117_trigger_conversion();
            htu31d_trigger_conversion();

            have_temp_for_log = false;
            have_hum_for_log = false;
        }

        if (local_tmp117_alert) {
            int16_t temp;
            int res = tmp117_read_temp(&temp);

            if (res == 0) {
                last_temp = temp;
            }

            have_temp_for_log = true;

            if (!transmitting_log) {
                uart_puts("t");
                if (res != 0) {
                    uart_puts("ERR");
                } else {
                    uart_print_u16_hex((uint16_t) temp);
                }
                uart_puts("\r\n");
            }
        }

        if (local_read_humidity) {
            int16_t rh;
            int res = htu31d_read_humidity(&rh);

            if (res == 0) {
                last_rh = rh;
            }

            have_hum_for_log = true;

            if (!transmitting_log) {
                uart_puts("h");
                if (res != 0) {
                    uart_puts("ERR");
                } else {
                    uart_print_u16_hex((uint16_t) rh);

                    last_rh = rh;
                }
                uart_puts("\r\n");
            }
        }

        if (!transmitting_log) {
            if (local_button_pressed == BTN_PRESS_SHORT) {
                uart_puts("BTN S\r\n");
            } else if (local_button_pressed == BTN_PRESS_LONG) {
                uart_puts("BTN L\r\n");
            }
        }

        switch (disp_state) {
            case DISP_STATE_TEMP:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_RH;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    disp_state = DISP_STATE_HOLD_TEMP;
                }
                break;
            case DISP_STATE_RH:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_HEATINDEX;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    disp_state = DISP_STATE_HOLD_RH;
                }
                break;
            case DISP_STATE_HEATINDEX:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_TEMP;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    disp_state = DISP_STATE_HOLD_HEATINDEX;
                }
                break;

            case DISP_STATE_HOLD_TEMP:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_TEMP;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    disp_state = DISP_STATE_MENU_DL_APPEND;
                }
                break;
            case DISP_STATE_HOLD_RH:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_RH;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    disp_state = DISP_STATE_MENU_DL_APPEND;
                }
                break;
            case DISP_STATE_HOLD_HEATINDEX:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_HEATINDEX;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    disp_state = DISP_STATE_MENU_DL_APPEND;
                }
                break;

            case DISP_STATE_MENU_DL_APPEND:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_NEW;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_append = true;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_TYPE_TEMP;
                }
                break;
            case DISP_STATE_MENU_DL_NEW:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_TRANSMIT;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_append = false;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_TYPE_TEMP;
                }
                break;
            case DISP_STATE_MENU_DL_TRANSMIT:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_BACK;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    transmitting_log = true;
                    uart_putc('>');
                    datalog_initiate_transmit();
                }
                break;
            case DISP_STATE_MENU_BACK:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_APPEND;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    // TODO: remember state before menu (hold/live?)
                    disp_state = DISP_STATE_TEMP;
                }
                break;

            case DISP_STATE_MENU_DL_OPTIONS_TYPE_TEMP:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_TYPE_RH;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_type = DL_TYPE_TEMP;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_TYPE_RH:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_TYPE_BOTH;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_type = DL_TYPE_HUM;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_TYPE_BOTH:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_TYPE_BACK;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_type = DL_TYPE_BOTH;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_TYPE_BACK:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_TYPE_TEMP;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    // TODO: go back to chosen option
                    disp_state = DISP_STATE_MENU_DL_APPEND;
                }
                break;

            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_2S;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_interval = DL_INTERVAL_1S;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_2S:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_5S;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_interval = DL_INTERVAL_2S;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_5S:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_10S;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_interval = DL_INTERVAL_5S;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_10S:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_30S;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_interval = DL_INTERVAL_10S;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_30S:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_60S;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_interval = DL_INTERVAL_30S;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_60S:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_BACK;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    datalog_interval = DL_INTERVAL_60S;

                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_BACK:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    // TODO: go back to chosen option
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_TYPE_TEMP;
                }
                break;

            case DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_BACK;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    if (!datalog_append) {
                        datalog_clear();
                    }
                    uart_puts("Interval: ");
                    uart_print_u8_hex(datalog_interval);
                    uart_puts(", type: ");
                    uart_print_u8_hex(datalog_type);
                    uart_puts("\r\n");
                    datalog_start(datalog_interval, datalog_type);

                    disp_state = DISP_STATE_TEMP;
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_CONFIRM_BACK:
                if (local_button_pressed == BTN_PRESS_SHORT) {
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO;
                } else if (local_button_pressed == BTN_PRESS_LONG) {
                    // TODO: go back to chosen option
                    disp_state = DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S;
                }
                break;
        }

        switch (disp_state) {
            case DISP_STATE_TEMP:
                disp_temp(last_temp);
                break;
            case DISP_STATE_RH:
                disp_rh(last_rh);
                break;
            case DISP_STATE_HEATINDEX:
                disp_heatindex(last_temp, last_rh);
                break;

            case DISP_STATE_HOLD_TEMP:
                break;
            case DISP_STATE_HOLD_RH:
                break;
            case DISP_STATE_HOLD_HEATINDEX:
                break;

            case DISP_STATE_MENU_DL_APPEND:
                lcd_show_sign(LCD_SIGN_PLUS);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_L);
                lcd_display_character(2, LCD_CHAR_O);
                lcd_display_character(1, LCD_CHAR_G);
                lcd_display_character(0, LCD_CHAR_SPACE);
                break;
            case DISP_STATE_MENU_DL_NEW:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_L);
                lcd_display_character(2, LCD_CHAR_O);
                lcd_display_character(1, LCD_CHAR_G);
                lcd_display_character(0, LCD_CHAR_SPACE);
                break;
            case DISP_STATE_MENU_DL_TRANSMIT:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_T);
                lcd_display_character(2, LCD_CHAR_R);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_SPACE);
                break;
            case DISP_STATE_MENU_BACK:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_B);
                lcd_display_character(2, LCD_CHAR_A);
                lcd_display_character(1, LCD_CHAR_C);
                lcd_display_character(0, LCD_CHAR_K);
                break;

            case DISP_STATE_MENU_DL_OPTIONS_TYPE_TEMP:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_T);
                lcd_display_character(2, LCD_CHAR_E);
                lcd_display_character(1, LCD_CHAR_M);
                lcd_display_character(0, LCD_CHAR_P);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_TYPE_RH:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_R);
                lcd_display_character(2, LCD_CHAR_H);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_SPACE);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_TYPE_BOTH:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_B);
                lcd_display_character(2, LCD_CHAR_O);
                lcd_display_character(1, LCD_CHAR_T);
                lcd_display_character(0, LCD_CHAR_H);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_TYPE_BACK:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_B);
                lcd_display_character(2, LCD_CHAR_A);
                lcd_display_character(1, LCD_CHAR_C);
                lcd_display_character(0, LCD_CHAR_K);
                break;

            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_1S:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_SPACE);
                lcd_display_digit(2, 1);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_S);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_2S:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_SPACE);
                lcd_display_digit(2, 2);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_S);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_5S:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_SPACE);
                lcd_display_digit(2, 5);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_S);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_10S:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_digit(3, 1);
                lcd_display_digit(2, 0);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_S);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_30S:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_digit(3, 3);
                lcd_display_digit(2, 0);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_S);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_60S:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_digit(3, 6);
                lcd_display_digit(2, 0);
                lcd_display_character(1, LCD_CHAR_SPACE);
                lcd_display_character(0, LCD_CHAR_S);
                break;
            case DISP_STATE_MENU_DL_OPTIONS_INTERVAL_BACK:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_B);
                lcd_display_character(2, LCD_CHAR_A);
                lcd_display_character(1, LCD_CHAR_C);
                lcd_display_character(0, LCD_CHAR_K);
                break;

            case DISP_STATE_MENU_DL_OPTIONS_CONFIRM_GO:
                if (local_trigger_conversion) {
                    dl_confirm_show_go = !dl_confirm_show_go;
                }

                if (dl_confirm_show_go) {
                    lcd_show_sign(LCD_SIGN_NONE);
                    lcd_show_dots(0);
                    lcd_display_character(3, LCD_CHAR_G);
                    lcd_display_character(2, LCD_CHAR_O);
                    lcd_display_character(1, LCD_CHAR_SPACE);
                    lcd_display_character(0, LCD_CHAR_SPACE);
                } else {
                    uint16_t hours = datalog_estimate_duration(datalog_append, datalog_type, datalog_interval);

                    lcd_show_sign(LCD_SIGN_NONE);
                    lcd_display_number(hours, 0, 1);
                    lcd_display_character(0, LCD_CHAR_H);
                }
                break;
            case DISP_STATE_MENU_DL_OPTIONS_CONFIRM_BACK:
                lcd_show_sign(LCD_SIGN_NONE);
                lcd_show_dots(0);
                lcd_display_character(3, LCD_CHAR_B);
                lcd_display_character(2, LCD_CHAR_A);
                lcd_display_character(1, LCD_CHAR_C);
                lcd_display_character(0, LCD_CHAR_K);
                break;
        }

        lcd_show_arrow(datalog_logging());

        if (have_temp_for_log && have_hum_for_log) {
            have_temp_for_log = false;
            have_hum_for_log = false;

            datalog_log(last_temp, last_rh);
        }

        if (transmitting_log) {
            bool done = datalog_transmit();
            if (done) {
                size_t written = uart_putc('<');
                if (written > 0) {
                    transmitting_log = false;
                }
            }
        }

        // TODO: LPM3 should be possible some of the time (derive Watchdog, LCD timer from 32kHz ACLK)
        //  might be possible to rely on 'clocks-on-demand' feature, so that LPM3 is entered automatically when no periph needs SMCLK?
        __low_power_mode_3();
    }
}

__attribute__((interrupt(PORT1_VECTOR)))
void isr_PORT1(void) {
    uint16_t source = P1IV;
    switch (source) {
        case P1IV__P1IFG6: // P1.6 (VBUS detect)
            P1IES ^= BIT6;

            usb_connected = P1IES & BIT6;

            // TODO: enable/disable UART logging

            __low_power_mode_off_on_exit();
            break;
    }
}

__attribute__((interrupt(PORT2_VECTOR)))
void isr_PORT2(void) {
    uint16_t source = P2IV;
    switch (source) {
        case P2IV__P2IFG2: // P2.2 (TMP117 ALERT)
            tmp117_alert = true;

            __low_power_mode_off_on_exit();
            break;
    }
}

static volatile uint16_t button_press_counter = 0;
static volatile bool button_press_done = false;

__attribute__((interrupt(PORT3_VECTOR)))
void isr_PORT3(void) {
    uint16_t source = P3IV;
    switch (source) {
        case P3IV__P3IFG5: // P3.5 (Button)
            // Disable interrupts for this pin until we can be sure it stopped bouncing
            P3IE &= ~BIT5;

            bool btn_edge_rising = P3IES & BIT5;
            P3IES ^= BIT5;

            // Stop timer and reset counter and clock divider registers
            TA1CTL &= ~MC;
            TA1CTL |= TACLR;
            // Start timer to re-enable interrupt in 25ms
            TA1CTL |= MC__UP;

            if (btn_edge_rising) {
                button_press_counter = 0;
                button_press_done = false;
            } else {
                if (button_press_counter < BTN_PRESS_COUNT_LONG) {
                    button_pressed = BTN_PRESS_SHORT;
                    button_press_done = true;
                }
            }

            __low_power_mode_off_on_exit();
            break;
    }
}

__attribute__((interrupt(TIMER1_A0_VECTOR)))
void isr_TIMER_A1_CCIFG0(void) {
    // Re-enable interrupts for the button
    if (!(P3IE & BIT5)) {
        P3IFG &= ~BIT5;
        P3IE |= BIT5;
    }

    if (!button_press_done) {
        button_press_counter++;
        if (button_press_counter >= BTN_PRESS_COUNT_LONG) {
            button_pressed = BTN_PRESS_LONG;

            __low_power_mode_off_on_exit();

            button_press_done = true;
        }
    }

    if (button_press_done) {
        // Stop timer and reset counter and clock divider registers
        TA1CTL &= ~MC;
        TA1CTL |= TACLR;
    }
}

__attribute__((interrupt(TIMER2_A0_VECTOR)))
void isr_TIMER_A2_CCIFG0(void) {
    trigger_conversion = true;

    __low_power_mode_off_on_exit();
}

__attribute__((interrupt(TIMER2_A1_VECTOR)))
void isr_TIMER_A2_TAxIV(void) {
    uint16_t source = TA2IV;
    switch (source) {
        case TAIV__TACCR1:
            read_humidity = true;

            __low_power_mode_off_on_exit();
            break;
    }
}
