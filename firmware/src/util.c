
#include "util.h"

void decimal_to_digits(int16_t value, size_t shift, size_t digit_count, ssize_t ones_index, uint8_t *out) {
    if (value < 0) {
        value = -value;
    }

    uint16_t v_int = value >> shift;
    uint16_t v_frac = value & ((1 << shift) - 1);

    if (ones_index < digit_count) {
        ssize_t i = ones_index;
        if (v_int > 0) {
            while (v_int > 0 && i < digit_count) {
                if (i >= 0) {
                    uint8_t digit = v_int % 10;
                    out[i] = digit;
                }
                v_int = v_int / 10;
                i++;
            }
        } else {
            out[i] = 0;
            i++;
        }

        while (i < digit_count) {
            out[i] = 10;
            i++;
        }
    }

    if (ones_index > 0) {
        ssize_t i = ones_index - 1;
        while (v_frac > 0 && i >= 0) {
            uint16_t tmp = v_frac * 10;
            uint8_t digit = tmp >> shift;
            if (i < digit_count) {
                out[i] = digit;
            }
            v_frac = tmp - (digit << shift);
            i--;
        }

        while (i >= 0) {
            out[i] = 0;
            i--;
        }
    }
}
