
#include "i2c.h"

#include <msp430.h>

/**
 * P1.2: UCB0SDA (I2C to sensors)
 * P1.3: UCB0SCL (I2C to sensors)
 */
void i2c_init_io() {
    P1SEL0 |= BIT2 | BIT3;
}

void i2c_init() {
    UCB0CTLW0 = UCSWRST;
    UCB0CTLW0 |= UCMODE_3 | UCMST | UCSSEL__SMCLK | UCSYNC;
    // fSCL = SMCLK/20 = ~400kHz
    UCB0BRW = 20;
    UCB0CTLW0 &= ~UCSWRST;
}

i2c_result_t i2c_write(uint8_t addr, size_t len, uint8_t *data) {
    if (len == 0) {
        return I2C_LEN_INVALID;
    }

    // Ensure STOP condition of the previous transmission has been sent
    while (UCB0CTLW0 & UCTXSTP)
        ;

    UCB0IFG &= ~UCTXIFG0;

    UCB0I2CSA = addr;
    // Direction: transmit
    UCB0CTLW0 |= UCTR;

    // Generate START and transmit adress and R/W bit
    UCB0CTLW0 |= UCTXSTT;

    // Wait until eUSCI_B is ready for the first data byte
    while (!(UCB0IFG & UCTXIFG0))
        ;

    // eUSCI_B stalls the bus and won't update UCTXSTT and UCNACKIFG until the first data byte is available
    UCB0TXBUF = data[0];

    // Wait until address has been sent before checking UCNACKIFG
    while (UCB0CTLW0 & UCTXSTT)
        ;

    // React with STOP if no slave ACK'ed the address
    if (UCB0IFG & UCNACKIFG) {
        UCB0CTLW0 |= UCTXSTP;
        return I2C_ADDR_NACK;
    }

    // Skip the first byte which has already been sent
    for (size_t i = 1; i < len; i++) {
        // UCTXIFG0 is set when UCB0TXBUF is ready for the next data byte
        while (!(UCB0IFG & UCTXIFG0))
            ;

        // Writing to UCB0TXBUF automatically clears UCTXIFG0
        UCB0TXBUF = data[i];
    }

    // Wait until transmission of the last byte has begun before setting UCTXSTP
    while (!(UCB0IFG & UCTXIFG0))
        ;

    // Generate STOP
    UCB0CTLW0 |= UCTXSTP;

    return I2C_OK;
}

i2c_result_t i2c_write_u8(uint8_t addr, uint8_t data) {
    return i2c_write(addr, 1, &data);
}

i2c_result_t i2c_write_u8_u16_be(uint8_t addr, uint8_t data0, uint16_t data1) {
    uint8_t bytes[3];
    bytes[0] = data0;
    bytes[1] = data1 >> 8;
    bytes[2] = data1 & 0xff;
    return i2c_write(addr, 3, bytes);
}

i2c_result_t i2c_read(uint8_t addr, size_t len, uint8_t *data_out) {
    if (len == 0) {
        return I2C_LEN_INVALID;
    }

    // Ensure STOP condition of the previous transmission has been sent
    while (UCB0CTLW0 & UCTXSTP)
        ;

    UCB0IFG &= ~UCRXIFG0;

    UCB0I2CSA = addr;
    // Direction: receive
    UCB0CTLW0 &= ~UCTR;

    // Generate START and transmit adress and R/W bit
    UCB0CTLW0 |= UCTXSTT;

    // Wait at least until address has been sent before setting UCTXSTP
    while (UCB0CTLW0 & UCTXSTT)
        ;

    // React with STOP if no slave ACK'ed the address
    if (UCB0IFG & UCNACKIFG) {
        UCB0CTLW0 |= UCTXSTP;
        return I2C_ADDR_NACK;
    }

    for (size_t i = 0; i < len; i++) {
        // UCTXSTP has to be set during reception of the last byte
        if (i == len - 1) {
            UCB0CTLW0 |= UCTXSTP;
        }

        // Wait until the next byte has been received
        while (!(UCB0IFG & UCRXIFG0))
            ;

        // Reading UCB0RXBUF automatically clears UCRXIFG0
        data_out[i] = UCB0RXBUF;
    }

    return I2C_OK;
}

i2c_result_t i2c_read_u8(uint8_t addr, uint8_t *data_out) {
    return i2c_read(addr, 1, data_out);
}

i2c_result_t i2c_read_u16_be(uint8_t addr, uint16_t *data_out) {
    uint8_t bytes[2];
    i2c_result_t res = i2c_read(addr, 2, bytes);
    if (res != I2C_OK) {
        return res;
    }
    *data_out = (uint16_t) bytes[0] << 8 | bytes[1];
    return I2C_OK;
}

// TODO: impl i2c_write_read using I2C Re-START
