
#include "lcd.h"

#include <msp430.h>
#include <stdbool.h>

#define LCD_COM_INDEX 8
#define LCD_COLON_INDEX 21
#define LCD_BATT_INDEX 9
#define LCD_MINUS_INDEX 6
#define LCD_PLUS_REST_INDEX 7
#define LCD_ARROW_INDEX 0

#define LCD_SHIFT_REG_COUNT 5

static const uint8_t LCD_DIGIT_INDICES[4][7] = {
//   A,  B,  C,  D,  E,  F,  G
    {33, 34, 38, 31, 30, 32, 39}, // 10^0
    {19, 18, 28, 27, 26, 20, 17}, // 10^1
    {16, 23, 24, 15, 14,  1, 22}, // 10^2
    { 4,  3, 12, 11, 10,  5,  2}  // 10^3
};

static const uint8_t LCD_DOT_INDICES[3] = {29, 25, 13};

static const uint8_t DIGIT_SEGMENTS[11] = {
//  0bGFEDCBA
    0b0111111, // 0
    0b0000110, // 1
    0b1011011, // 2
    0b1001111, // 3
    0b1100110, // 4
    0b1101101, // 5
    0b1111101, // 6
    0b0000111, // 7
    0b1111111, // 8
    0b1101111, // 9
    0b0000000  // ' '
};

static uint8_t lcd_segments[LCD_SHIFT_REG_COUNT];

static volatile bool update_lcd = false;

static bool lcd_inverted = false;

static void lcd_set_segment(uint8_t index, bool state);

/**
 * P2.4: UCA1CLK ("SPI" to shift registers)
 * P2.6: UCA1SIMO ("SPI" to shift registers)
 * P3.1: Output, active-low (Display OE)
 * P3.4: Output (Display Latch)
 */
void lcd_init_io() {
    P2SEL0 |= BIT4 | BIT6;

    P3OUT |= BIT1;
}

void lcd_init() {
    // TODO: init SPI peripheral for loading data into the shift registers
    UCA1CTLW0 = UCSWRST;
    // MSB first, 8 bit
    UCA1CTLW0 |= UCSSEL__SMCLK | UCSYNC | UCMODE_0 | UCMST | UCMSB;
    //
    UCA1BRW = 0;
    UCA1CTLW0 &= ~UCSWRST;

    // Timer_A 0 is used to generate the AC pattern for the LCD
    TA0CTL = TACLR;
    // Timer clock is ACLK/64 = 512Hz
    TA0CTL |= ID__8 | TASSEL__ACLK;
    TA0EX0 = TAIDEX__8;
    TA0CCTL0 = CCIE;
    // Set up for 512Hz / 5 = 102.4Hz
    TA0CCR0 = (5 - 1);

    lcd_set_segment(LCD_COM_INDEX, false);
}

void lcd_enable() {
    TA0CTL |= MC__UP;
    // The shift register outputs will be enabled in the timer ISR
}

void lcd_disable() {
    TA0CTL |= TACLR;
    P3OUT |= BIT1;
}

void lcd_load_data(bool invert) {
    // for (size_t i = 0; i < LCD_SHIFT_REG_COUNT; i++) {
    //     while (!(UCA1IFG & UCTXIFG))
    //         ;

    //     UCA1TXBUF = lcd_segments[i];
    // }

    size_t i = LCD_SHIFT_REG_COUNT;
    while (i > 0) {
        i--;

        while (!(UCA1IFG & UCTXIFG))
            ;

        uint8_t segments = lcd_segments[i];
        uint8_t value = invert ? ~segments : segments;

        UCA1TXBUF = value;
    }
}

static void lcd_buffer_latch() {
    P3OUT |= BIT4;
    __delay_cycles(1);
    P3OUT &= ~BIT4;
}

static void lcd_set_segment(uint8_t index, bool state) {
    uint8_t byte_index = index / 8;
    uint8_t bit_index = index % 8;
    uint8_t mask = 1 << bit_index;

    uint8_t value = lcd_segments[byte_index];
    //value = (value & ~mask) | (state << bit_index); // TODO: might be faster?
    if (state) {
        value |= mask;
    } else {
        value &= ~mask;
    }
    lcd_segments[byte_index] = value;
}

void lcd_display_clear() {
    for (size_t i = 0; i < LCD_SHIFT_REG_COUNT; i++) {
        lcd_segments[i] = 0;
    }

    // Ensure COM is low
    lcd_set_segment(LCD_COM_INDEX, false);
}

void lcd_all_on() {
    for (size_t i = 0; i < LCD_SHIFT_REG_COUNT; i++) {
        lcd_segments[i] = 0xff;
    }

    // Ensure COM is low
    lcd_set_segment(LCD_COM_INDEX, false);
}

void lcd_show_colon(bool show) {
    lcd_set_segment(LCD_COLON_INDEX, show);
}

void lcd_show_batt(bool show) {
    lcd_set_segment(LCD_BATT_INDEX, show);
}

void lcd_show_arrow(bool show) {
    lcd_set_segment(LCD_ARROW_INDEX, show);
}

void lcd_show_sign(enum lcd_sign sign) {
    lcd_set_segment(LCD_MINUS_INDEX, sign == LCD_SIGN_MINUS || sign == LCD_SIGN_PLUS);
    lcd_set_segment(LCD_PLUS_REST_INDEX, sign == LCD_SIGN_PLUS);
}

void lcd_show_dots(uint8_t dots) {
    for (size_t i = 0; i < 3; i++) {
        uint8_t value = (dots >> i) & 1;
        lcd_set_segment(LCD_DOT_INDICES[i], value);
    }
}

void lcd_show_dot(uint8_t decimal_pos) {
    uint8_t dots;
    switch (decimal_pos) {
        case 1: dots = LCD_DOT_0; break;
        case 2: dots = LCD_DOT_1; break;
        case 3: dots = LCD_DOT_2; break;
        default: dots = 0;
    }
    lcd_show_dots(dots);
}

void lcd_display_character(uint8_t char_index, uint8_t character) {
    for (size_t i = 0; i < 7; i++) {
        lcd_set_segment(LCD_DIGIT_INDICES[char_index][i], (character >> i) & 1);
    }
}

void lcd_display_digit(uint8_t char_index, uint8_t digit) {
    lcd_display_character(char_index, DIGIT_SEGMENTS[digit]);
}

static void lcd_generate_digits(uint8_t digits[4]) {
    for (size_t i = 0; i < 4; i++) {
        uint8_t digit = digits[i];
        lcd_display_digit(i, digit);
    }
}

void lcd_display_number(int16_t num, uint8_t decimal_pos, uint8_t where) {
    bool negative = false;
    if (num < 0) {
        negative = true;
        num = -num;
    }

    size_t min_digit_count = decimal_pos + 1;

    uint8_t digits[4];

    uint8_t i = where;
    while (num > 0 && i < 4) {
        int16_t digit = num % 10;
        num /= 10;
        digits[i] = digit;
        i++;
    }

    while (i < min_digit_count) {
        digits[i] = 0;
        i++;
    }

    while (i < 4) {
        digits[i] = 10;
        i++;
    }

    lcd_generate_digits(digits);

    lcd_show_sign(negative ? LCD_SIGN_MINUS : LCD_SIGN_NONE);

    lcd_show_dot(decimal_pos);
}

void lcd_display_frac(int16_t v, size_t shift, ssize_t ones_index) {
    uint8_t digits[4];

    decimal_to_digits(v, shift, 4, ones_index, digits);

    lcd_generate_digits(digits);

    lcd_show_sign((v < 0) ? LCD_SIGN_MINUS : LCD_SIGN_NONE);

    lcd_show_dot(ones_index);
}

void lcd_update() {
    if (update_lcd) {
        update_lcd = false;

        lcd_load_data(lcd_inverted);

        lcd_inverted = !lcd_inverted;
    }
}

__attribute__((interrupt(TIMER0_A0_VECTOR)))
void isr_TIMER_A0_CCIFG0(void) {
    lcd_buffer_latch();

    // Ensure shift register outputs are enabled, which should only actually not be the case the first time this interrupt happens
    // OE is active-low
    P3OUT &= ~BIT1;

    update_lcd = true;

    __low_power_mode_off_on_exit();
}
