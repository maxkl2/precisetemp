
#pragma once

#include <stdint.h>

#define TMP117_TEMP_SHIFT 7

int tmp117_init();
int tmp117_trigger_conversion();
int tmp117_read_temp(int16_t *temp_out);
int16_t tmp117_int2temp(int16_t v);
