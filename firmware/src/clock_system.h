
#pragma once

#define MCLK_FREQ_MHZ 8 // MCLK = 8MHz
#define F_CPU (1000000 * MCLK_FREQ_MHZ)
#define T_MS (F_CPU / 1000)

void init_clocks();
