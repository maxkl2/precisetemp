
#include "tmp117.h"

#include <msp430.h>
#include <stdbool.h>

#include "i2c.h"

#define I2C_ADDR_TMP117 0x48

#define TMP117_REG_TEMP 0x00
#define TMP117_REG_CONFIG 0x01
#define TMP117_REG_ID 0x0f

static uint16_t current_config;

int tmp117_init() {
    i2c_result_t i2c_res;

    i2c_res = i2c_write_u8(I2C_ADDR_TMP117, TMP117_REG_ID);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    uint16_t dev_id;
    i2c_res = i2c_read_u16_be(I2C_ADDR_TMP117, &dev_id);
    if (i2c_res != I2C_OK) {
        return 1;
    }
    if (dev_id != 0x0117) {
        return 1;
    }

    i2c_res = i2c_write_u8(I2C_ADDR_TMP117, TMP117_REG_CONFIG);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    bool eeprom_busy;
    do {
        uint8_t config_reg_h;
        i2c_res = i2c_read_u8(I2C_ADDR_TMP117, &config_reg_h);
        if (i2c_res != I2C_OK) {
            return 1;
        }
        eeprom_busy = config_reg_h & 0x10;
    } while (eeprom_busy);

    // ALERT: data ready, AVG: 8 averages, CONV: 1s cycle time, MOD: continuous conversion
    // uint16_t config = 0x0224;
    // ALERT: data ready, AVG: 8 averages, MOD: shutdown
    uint16_t config = 0x0424;

    i2c_res = i2c_write_u8_u16_be(I2C_ADDR_TMP117, TMP117_REG_CONFIG, config);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    uint16_t config_readback;
    i2c_res = i2c_read_u16_be(I2C_ADDR_TMP117, &config_readback);
    if (i2c_res != I2C_OK) {
        return 1;
    }
    if ((config_readback & 0x0fff) != config) {
        return 1;
    }

    current_config = config;

    return 0;
}

int tmp117_trigger_conversion() {
    i2c_result_t i2c_res;

    // Set MOD to 0b11 (One-shot conversion)
    uint16_t config = (current_config & ~(0b11 << 10)) | (0b11 << 10);

    i2c_res = i2c_write_u8_u16_be(I2C_ADDR_TMP117, TMP117_REG_CONFIG, config);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    uint16_t config_readback;
    i2c_res = i2c_read_u16_be(I2C_ADDR_TMP117, &config_readback);
    if (i2c_res != I2C_OK) {
        return 1;
    }
    if ((config_readback & 0x0fff) != config) {
        return 1;
    }

    current_config = config;

    return 0;
}

int tmp117_read_temp(int16_t *temp_out) {
    i2c_result_t i2c_res;

    i2c_res = i2c_write_u8(I2C_ADDR_TMP117, TMP117_REG_TEMP);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    uint16_t temp_raw;
    i2c_res = i2c_read_u16_be(I2C_ADDR_TMP117, &temp_raw);
    if (i2c_res != I2C_OK) {
        return 1;
    }

    *temp_out = (int16_t) temp_raw;

    return 0;
}

int16_t tmp117_int2temp(int16_t v) {
    return v << TMP117_TEMP_SHIFT;
}
