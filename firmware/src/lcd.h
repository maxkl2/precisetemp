
#pragma once

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "util.h"

// 0bGFEDCBA
#define LCD_CHAR_SPACE 0b0000000
#define LCD_CHAR_A 0b1110111
#define LCD_CHAR_B 0b1111100
#define LCD_CHAR_C 0b1011000
#define LCD_CHAR_E 0b1111001
#define LCD_CHAR_F 0b1110001
#define LCD_CHAR_G 0b0111101
#define LCD_CHAR_H 0b1110100
#define LCD_CHAR_I 0b0000100
#define LCD_CHAR_K 0b1110101
#define LCD_CHAR_L 0b0111000
#define LCD_CHAR_M 0b0110111
#define LCD_CHAR_N 0b1010100
#define LCD_CHAR_O 0b1011100
#define LCD_CHAR_P 0b1110011
#define LCD_CHAR_Q 0b1100111
#define LCD_CHAR_R 0b1010000
#define LCD_CHAR_S 0b1101100
#define LCD_CHAR_T 0b1111000
#define LCD_CHAR_U 0b0011100

#define LCD_DOT_0 (1 << 0)
#define LCD_DOT_1 (1 << 1)
#define LCD_DOT_2 (1 << 2)

enum lcd_sign {
    LCD_SIGN_NONE = 0,
    LCD_SIGN_MINUS,
    LCD_SIGN_PLUS
};

void lcd_init_io();
void lcd_init();
void lcd_enable();
void lcd_disable();
void lcd_load_data(bool invert);
void lcd_display_clear();
void lcd_all_on();
void lcd_show_colon(bool show);
void lcd_show_batt(bool show);
void lcd_show_arrow(bool show);
void lcd_show_sign(enum lcd_sign sign);
void lcd_show_dots(uint8_t dots);
void lcd_show_dot(uint8_t decimal_pos);
void lcd_display_character(uint8_t char_index, uint8_t character);
void lcd_display_digit(uint8_t char_index, uint8_t digit);
void lcd_display_number(int16_t num, uint8_t decimal_pos, uint8_t where);
void lcd_display_frac(int16_t v, size_t shift, ssize_t ones_index);
void lcd_update();
