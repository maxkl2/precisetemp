
#pragma once

#include <stdbool.h>
#include <stdint.h>

#define HTU31D_RH_SHIFT 8

void htu31d_init_io();
int htu31d_init();
int16_t htu31d_int2rh(int16_t v);
int htu31d_trigger_conversion();
int htu31d_read_humidity(int16_t *humidity_out);
