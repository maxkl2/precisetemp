
#pragma once

#include <stdint.h>
#include <stdbool.h>

enum datalog_interval {
    DL_INTERVAL_1S = 0,
    DL_INTERVAL_2S,
    DL_INTERVAL_5S,
    DL_INTERVAL_10S,
    DL_INTERVAL_30S,
    DL_INTERVAL_60S
};

enum datalog_type {
    DL_TYPE_TEMP = 0,
    DL_TYPE_HUM,
    DL_TYPE_BOTH
};

void datalog_init();
void datalog_clear();
void datalog_start(enum datalog_interval interval, enum datalog_type type);
bool datalog_logging();
void datalog_log(int16_t temp, int16_t hum);
uint16_t datalog_estimate_duration(bool append, enum datalog_type type, enum datalog_interval interval);
void datalog_initiate_transmit();
bool datalog_transmit();
void datalog_fill_dummy_data();
