
#pragma once

#include <stdint.h>

int8_t heatindex_get(uint8_t temp, uint8_t rh);
