
#include "heatindex.h"

#define HEATINDEX_RH_OFFSET 40
#define HEATINDEX_RH_STEP 5
#define HEATINDEX_RH_ROWS 13
#define HEATINDEX_TEMP_OFFSET 27
#define HEATINDEX_TEMP_STEP 1
#define HEATINDEX_TEMP_COLS 17
// Source: https://en.wikipedia.org/wiki/Heat_index#Table_of_values (interpolated values for 35 °C)
static const uint8_t heatindex_table[HEATINDEX_RH_ROWS][HEATINDEX_TEMP_COLS] = {
    {27,27,28,29,31,33,34,36,37,38,41,43,46,48,51,54,58},
    {27,28,29,31,32,34,36,38,39,40,43,46,48,51,54,58, 0},
    {27,28,29,31,33,35,37,39,41,42,45,48,51,55,58, 0, 0},
    {27,29,30,32,34,36,38,41,43,44,47,51,54,58, 0, 0, 0},
    {28,29,31,33,35,38,41,43,45,47,51,54,58, 0, 0, 0, 0},
    {28,29,32,34,37,39,42,46,48,49,53,58, 0, 0, 0, 0, 0},
    {28,30,32,35,38,41,44,48,50,52,57, 0, 0, 0, 0, 0, 0},
    {29,31,33,36,39,43,47,51,54,56, 0, 0, 0, 0, 0, 0, 0},
    {29,32,34,38,41,45,49,54,57, 0, 0, 0, 0, 0, 0, 0, 0},
    {29,32,36,39,43,47,52,57, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {30,33,37,41,45,50,55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {30,34,38,42,47,53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {31,35,39,44,49,56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

static unsigned div_round_closest(unsigned n, unsigned d) {
    return (n + d / 2) / d;
}

int8_t heatindex_get(uint8_t temp, uint8_t rh) {
    if (temp < HEATINDEX_TEMP_OFFSET || rh < HEATINDEX_RH_OFFSET) {
        return -1;
    }

    uint8_t col = div_round_closest(temp - HEATINDEX_TEMP_OFFSET, HEATINDEX_TEMP_STEP);
    if (col >= HEATINDEX_TEMP_COLS) {
        return 99;
    }

    uint8_t row = div_round_closest(rh - HEATINDEX_RH_OFFSET, HEATINDEX_RH_STEP);
    if (row >= HEATINDEX_RH_ROWS) {
        return 99;
    }

    uint8_t heatindex = heatindex_table[row][col];

    if (heatindex == 0) {
        return 99;
    }

    return heatindex;
}
